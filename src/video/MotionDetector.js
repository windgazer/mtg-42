// https://hackernoon.com/motion-detection-in-javascript-2614adea9325

const sample_size = 10;
const w = 240;
const h = 144;

function drawRect(ctx, r, g, b, x, y) {
  ctx.fillStyle = `rgb(${r}, ${g}, ${b})`;
  ctx.fillRect(x, y, sample_size, sample_size);
}

function draw(video, canvas){
  const ctx = canvas.getContext('2d')

  let compoundChange = 0;
  // Get existing frame
  const previousFrame = ctx.getImageData(0, 0, w, h).data;

  // draw video onto screen
  ctx.drawImage(video, 0, 0, w, h);

  // get the screen's pixels data
  var data = ctx.getImageData(0, 0, w, h).data;

  // loop through rows and columns
  for (var y = 0; y < h; y+= sample_size) {

    for (var x = 0; x < w; x+= sample_size) {

      // the data array is a continuous array of red, blue, green 
      // and alpha values, so each pixel takes up four values 
      // in the array
      var pos = (x + y * w) * 4;
      
      // get red, blue and green pixel value
      var r = data[pos];
      var g = data[pos+1];
      var b = data[pos+2];

      if (
        previousFrame[pos] &&
        Math.abs( previousFrame[pos] - r) > 50
      ) {
        const div = Math.abs( previousFrame[pos] - r);
        compoundChange += div;
      }

    }
  }

  if (compoundChange > 150) {
    const event = new Event('VideoMotion');
    event.motion = {
      compoundChange,
      canvas,
    }
    video.dispatchEvent(event);
  }
}

function createCanvas(w=300, h=200) {
  const div = document.createElement('div');
  const canvas = document.createElement('canvas');
  div.style.cssText = 'position:absolute;height:0;height:0;top:0;left:0;overflow:hidden;';
  canvas.style.cssText = 'width:${w}px;height:${h}px;';
  div.appendChild(canvas);
  document.body.appendChild(div);
  return canvas;
}

function detectMotion(
  video = HTMLVideoElement,
  canvas,
) {
  if (!canvas) {
    return detectMotion(
      video,
      createCanvas(),
    )
  }
  draw(video, canvas);
  requestAnimationFrame(() => detectMotion(video, canvas));
}

export {
  detectMotion,
};
