import { detectMotion } from './MotionDetector';

function runVideo(video = HTMLVideoElement) {
    if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true })
            .then( (stream) => {
                console.log('starting the stream!!');
                video.srcObject = stream;
            })
            .catch((e) => {
                console.exception(e);
            })
        ;
    }
}

export {
    runVideo,
    detectMotion,
};
