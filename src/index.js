import {
    runVideo,
    detectMotion,
} from './video';

const debounce = (() => {
    let timeout;

    return (
        func = () => { throw 'No handler!!'; },
        delay = 1000
    ) => {
        if (timeout) {
            window.clearTimeout(timeout);
            timeout = null;
        }
        timeout = window.setTimeout(func, delay);
        return timeout;
    }
})();

window.addEventListener( 'DOMContentLoaded', () => {
    const video = document.querySelector(".media_container__media");
    video.addEventListener( "VideoMotion", (e) => {
        debounce(() => {
            //const url = e.motion.canvas.toDataURL();
            console.log(
                'No movement detected!',
                e.motion,
                e.target,
            );
        })
    });
    runVideo(video);
    detectMotion(video);
});
